import { Authentication } from "../../src/http/authentication";
import { request } from "../../src/http/http-request";

const HOMOLOG = "https://kwh5gituue.execute-api.sa-east-1.amazonaws.com/hlg/api/v1";
const TST_PARAMS = "inscricao=77866&codigoPlano=1&codigoInstituidor=1&cpf=01022255916";
const TST_ENDPOINT = `${HOMOLOG}/beneficio/participante/parcelasVencidas?${TST_PARAMS}`;
//
const USERNAME = "mobile";
const PASSWORD = "qp%2009TESTEm";
const ENDPOINT = `${HOMOLOG}/acesso/login`;

describe("authentication", () => {
  test("must execute with success without token", async (done) => {
    const authentication = new Authentication(USERNAME, PASSWORD, ENDPOINT);
    const data = await authentication.authenticate(async (token) => {
      const { body } = (await request().get(TST_ENDPOINT, {
        headers: {
          authorization: `bearer ${token}`,
        }, json: true,
      }));
      return body;
    });
    expect(data.informations[0].code).toBe(200);
    done();
  });
  test("must execute with success with invalid token", async (done) => {
    const authentication = new Authentication(USERNAME, PASSWORD, ENDPOINT);
    authentication.token = "<INVALID>";
    const data = await authentication.authenticate(async (token) => {
      const { body } = (await request().get(TST_ENDPOINT, {
        headers: {
          authorization: `bearer ${token}`,
        }, json: true,
      }));
      return body;
    });
    expect(data.informations[0].code).toBe(200);
    done();
  });
});
