import { APIGatewayProxyEvent, APIGatewayProxyResult, Callback, Context, Handler, SQSEvent } from "aws-lambda";
import { MessageBodyAttributeMap } from "aws-sdk/clients/sqs";
import { ObjectSchema } from "hapi__joi";
import { BAD_REQUEST, INTERNAL_SERVER_ERROR } from "http-status-codes";
import { EntityErrorCreator } from "..";
import { ERR_REQUIRED_MSG, ERR_UNKNOWN, ERR_UNKNOWN_MSG } from "./response/error-codes";
import { Response, ResponseCreator } from "./response/response";

const { error } = ResponseCreator;

const {
  ALLOW_ORIGIN, ALLOW_METHODS, ALLOW_HEADERS, ALLOW_CREDENTIALS,
} = process.env;

const baseHeaders = (headers: {} = {}) => {
  return {
    "Access-Control-Allow-Credentials": ALLOW_CREDENTIALS === null || ALLOW_CREDENTIALS === undefined ? true : false,
    "Access-Control-Allow-Headers": ALLOW_HEADERS || "*",
    "Access-Control-Allow-Methods": ALLOW_METHODS || "*",
    "Access-Control-Allow-Origin": ALLOW_ORIGIN || "*",
    "Content-Type": "application/json",
    ...headers,
  };
};

export function treated(exception: any): Response<any> {
  process.stderr.write(`Failure: ${exception}`);
  const status = exception.statusCode
    ? exception.statusCode
    : INTERNAL_SERVER_ERROR;
  return error(status, ERR_UNKNOWN_MSG, [{
    code: ERR_UNKNOWN,
    description: exception.message,
  }]);
}

export function validate<T>(data: T, schema: ObjectSchema): T {
  const result = schema.validate(data, { abortEarly: false });
  if (result.error) {
    throw error(BAD_REQUEST, ERR_REQUIRED_MSG,
      EntityErrorCreator.allOf(result.error.details),
    );
  }
  return data;
}

export type SqsOrHttpEvent = APIGatewayProxyEvent & SQSEvent;

export function convert<T>(event: SqsOrHttpEvent, type: new() => T): T {
  const target = new type();
  const body = event.Records && event.Records.length
    ? event.Records[0].body
    : event.body;
  Object.assign(target, JSON.parse(body));
  Object.keys(target)
        .forEach((key) => target[key] === undefined && delete target[key]);
  return target;
}

export type Content<Return> = Promise<Response<Return>>;

export type Endpoint<Event = any, Return = any> = (
  event: Event,
  context: Context,
  callback: Callback<Return>,
) => Content<Return>;

export function execute(endpoint: Endpoint): Handler {
  return async (event, context, cb) => {
    try {
      const result = await endpoint(event, context, cb);
      return callback(result);
    } catch (e) {
      return failure(e);
    }
  };
}

export function callback<T>(response: Response<T>): APIGatewayProxyResult {
  return {
    body: JSON.stringify(response.entity),
    headers: baseHeaders(),
    statusCode: response.status,
  };
}

export function failure<T>(response: any): APIGatewayProxyResult {
  if (response && isResponse(response)) {
    return callback(response);
  }
  return callback(error(INTERNAL_SERVER_ERROR, ERR_UNKNOWN_MSG, [{
    code: ERR_UNKNOWN,
    description: response.message
      ? response.message
      : ERR_REQUIRED_MSG,
  }]));
}

export function isResponse<T>(response: Response<T>): response is Response<T> {
  return response.attributeId !== undefined;
}

export function getSqsAttribute(event: SqsOrHttpEvent, attrKey: string): string {
  if (event.Records) {
    const record = event.Records[0];
    if (record && record.messageAttributes) {
      const attributes = record.messageAttributes as any as MessageBodyAttributeMap;
      const attrVal = attributes[attrKey] as any;
      return attrVal ? (attrVal.stringValue || attrVal.StringValue) : undefined;
    }
  }
}

export function getDirname(): string {
  const { LAMBDA_TASK_ROOT } = process.env;
  return LAMBDA_TASK_ROOT
    ? `${LAMBDA_TASK_ROOT}`
    : `${__dirname.split("/node_modules/")[0]}`;
}
