import * as moment from "moment-timezone";
import { ISerializer } from "../serializer";

const TZ = process.env.TZ || "America/Sao_Paulo";

export class DateSerializer implements ISerializer<Date, string> {
  public serialize(source: Date): string {
    if (source) {
      return moment(source)
        .tz(TZ)
        .format("YYYY-MM-DDTHH:mm:ss");
    }
  }
}
