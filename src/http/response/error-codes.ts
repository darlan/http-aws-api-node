export const ERR_REQUIRED = "ERR_REQUIRED";
export const ERR_REQUIRED_MSG = "Required";

export const ERR_UNKNOWN = "ERR_UNKNOWN";
export const ERR_UNKNOWN_MSG = "Unknown";
