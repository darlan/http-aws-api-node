import { ValidationErrorItem } from "hapi__joi";
import { ERR_REQUIRED } from "./error-codes";

export class EntityError {
  constructor(
    public code?: string,
    public description?: string,
    public message?: string,
  ) { }
}

export class EntityErrorCreator {
  public static create(detail: ValidationErrorItem): EntityError {
    return new EntityError(ERR_REQUIRED, detail.message,  detail.path.join("."));
  }

  public static allOf(details: ValidationErrorItem[]): EntityError[] {
    return details.map((detail) => EntityErrorCreator.create(detail));
  }
}
