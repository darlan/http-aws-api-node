export declare class Authentication {
    private username;
    private password;
    private endpoint;
    private _token?;
    constructor(username: string, password: string, endpoint: string);
    authenticate<T>(executable: (token: string) => Promise<T>): Promise<T>;
    token: string;
    private execute;
    private getToken;
}
