"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EntityResult {
    constructor(code, info, message) {
        this.code = code;
        this.info = info;
        this.message = message;
    }
}
exports.EntityResult = EntityResult;
class EntityResultCreator {
    static ofCodeAndInfoAndMessage(code, info, message) {
        return new EntityResult(code, info, message);
    }
    static ofCode(code) {
        return new EntityResult(code);
    }
}
exports.EntityResultCreator = EntityResultCreator;
//# sourceMappingURL=entity-result.js.map