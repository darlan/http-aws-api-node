export declare class EntityResult {
    code?: string;
    info?: string;
    message?: string;
    constructor(code?: string, info?: string, message?: string);
}
export declare class EntityResultCreator {
    static ofCodeAndInfoAndMessage(code: string, info: string, message: string): EntityResult;
    static ofCode(code: string): EntityResult;
}
