import { ValidationErrorItem } from "hapi__joi";
export declare class EntityError {
    code?: string;
    description?: string;
    message?: string;
    constructor(code?: string, description?: string, message?: string);
}
export declare class EntityErrorCreator {
    static create(detail: ValidationErrorItem): EntityError;
    static allOf(details: ValidationErrorItem[]): EntityError[];
}
