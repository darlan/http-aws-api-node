"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ERR_REQUIRED = "ERR_REQUIRED";
exports.ERR_REQUIRED_MSG = "Required";
exports.ERR_UNKNOWN = "ERR_UNKNOWN";
exports.ERR_UNKNOWN_MSG = "Unknown";
//# sourceMappingURL=error-codes.js.map