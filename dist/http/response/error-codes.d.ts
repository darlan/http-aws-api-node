export declare const ERR_REQUIRED = "ERR_REQUIRED";
export declare const ERR_REQUIRED_MSG = "Required";
export declare const ERR_UNKNOWN = "ERR_UNKNOWN";
export declare const ERR_UNKNOWN_MSG = "Unknown";
