"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const error_codes_1 = require("./error-codes");
class EntityError {
    constructor(code, description, message) {
        this.code = code;
        this.description = description;
        this.message = message;
    }
}
exports.EntityError = EntityError;
class EntityErrorCreator {
    static create(detail) {
        return new EntityError(error_codes_1.ERR_REQUIRED, detail.message, detail.path.join("."));
    }
    static allOf(details) {
        return details.map((detail) => EntityErrorCreator.create(detail));
    }
}
exports.EntityErrorCreator = EntityErrorCreator;
//# sourceMappingURL=entity-error.js.map