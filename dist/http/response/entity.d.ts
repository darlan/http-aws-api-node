import { EntityError } from "./entity-error";
import { EntityResult } from "./entity-result";
export declare class Entity<T> {
    result?: EntityResult;
    errors?: EntityError[];
    data?: T;
    constructor(result?: EntityResult, errors?: EntityError[], data?: T);
}
