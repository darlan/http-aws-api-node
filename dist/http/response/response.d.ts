import { Entity } from "./entity";
import { EntityError } from "./entity-error";
export declare class Response<T> {
    status?: number;
    entity?: Entity<T>;
    attributeId: string;
    constructor(status?: number, entity?: Entity<T>);
}
export declare class ResponseCreator {
    static success<T>(status: number, message: string, data: T): Response<T>;
    static error<T>(status: number, message: string, errors: EntityError[]): Response<T>;
}
