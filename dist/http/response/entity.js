"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Entity {
    constructor(result, errors, data) {
        this.result = result;
        this.errors = errors;
        this.data = data;
    }
}
exports.Entity = Entity;
//# sourceMappingURL=entity.js.map