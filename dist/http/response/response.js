"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = require("http-status-codes");
class Response {
    constructor(status, entity) {
        this.status = status;
        this.entity = entity;
        this.attributeId = "c1b57dce-3529-4c7e-8142-6ffaf627c222";
    }
}
exports.Response = Response;
class ResponseCreator {
    static success(status, message, data) {
        return new Response(status, {
            data: data ? data : undefined,
            result: {
                code: `${status}`,
                info: http_status_codes_1.getStatusText(status),
                message: message ? message : undefined,
            },
        });
    }
    static error(status, message, errors) {
        return new Response(status, {
            errors: errors && errors.length ? errors : undefined,
            result: {
                code: `${status}`,
                info: http_status_codes_1.getStatusText(status),
                message: message ? message : undefined,
            },
        });
    }
}
exports.ResponseCreator = ResponseCreator;
//# sourceMappingURL=response.js.map