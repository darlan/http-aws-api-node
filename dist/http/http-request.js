"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const got = require("got");
const ID_LENGTH = 36;
const ID_START = 2;
const ID_END = 15;
const BODY_SIZE_LIMIT = 2000;
function httpContextId() {
    return Math.random()
        .toString(ID_LENGTH)
        .substring(ID_START, ID_END) + Math.random()
        .toString(ID_LENGTH)
        .substring(ID_START, ID_END);
}
function getLoggable(raw, keys) {
    const data = keys.reduce((source, key) => (Object.assign({}, source, { [key]: raw[key] })), {});
    const body = data.body;
    if (body) {
        if (typeof body === "string") {
            if (body.length > BODY_SIZE_LIMIT) {
                data.body = body.substring(0, BODY_SIZE_LIMIT);
            }
        }
        else {
            data.body = "<<<<Binary>>>>";
        }
    }
    return data;
}
function beforeRequest() {
    return [
        (options) => {
            const raw = options;
            raw.httpContextId = httpContextId();
            const data = getLoggable(raw, [
                "httpContextId",
                "method",
                "protocol",
                "hostname",
                "path",
                "query",
                "headers",
                "body",
            ]);
            process.stdout.write(`${JSON.stringify(data)}\n`);
        },
    ];
}
function afterRequest() {
    return [
        (raw, _) => {
            const data = getLoggable(raw, [
                "statusCode",
                "headers",
                "body",
            ]);
            data.httpContextId = raw.request.gotOptions.httpContextId;
            process.stdout.write(`${JSON.stringify(data)}\n`);
            return raw;
        },
    ];
}
function request() {
    return got.extend({
        hooks: {
            afterResponse: afterRequest(),
            beforeRequest: beforeRequest(),
        },
    });
}
exports.request = request;
//# sourceMappingURL=http-request.js.map