"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_status_codes_1 = require("http-status-codes");
const __1 = require("..");
const error_codes_1 = require("./response/error-codes");
const response_1 = require("./response/response");
const { error } = response_1.ResponseCreator;
const { ALLOW_ORIGIN, ALLOW_METHODS, ALLOW_HEADERS, ALLOW_CREDENTIALS, } = process.env;
const baseHeaders = (headers = {}) => {
    return Object.assign({ "Access-Control-Allow-Credentials": ALLOW_CREDENTIALS === null || ALLOW_CREDENTIALS === undefined ? true : false, "Access-Control-Allow-Headers": ALLOW_HEADERS || "*", "Access-Control-Allow-Methods": ALLOW_METHODS || "*", "Access-Control-Allow-Origin": ALLOW_ORIGIN || "*", "Content-Type": "application/json" }, headers);
};
function treated(exception) {
    process.stderr.write(`Failure: ${exception}`);
    const status = exception.statusCode
        ? exception.statusCode
        : http_status_codes_1.INTERNAL_SERVER_ERROR;
    return error(status, error_codes_1.ERR_UNKNOWN_MSG, [{
            code: error_codes_1.ERR_UNKNOWN,
            description: exception.message,
        }]);
}
exports.treated = treated;
function validate(data, schema) {
    const result = schema.validate(data, { abortEarly: false });
    if (result.error) {
        throw error(http_status_codes_1.BAD_REQUEST, error_codes_1.ERR_REQUIRED_MSG, __1.EntityErrorCreator.allOf(result.error.details));
    }
    return data;
}
exports.validate = validate;
function convert(event, type) {
    const target = new type();
    const body = event.Records && event.Records.length
        ? event.Records[0].body
        : event.body;
    Object.assign(target, JSON.parse(body));
    Object.keys(target)
        .forEach((key) => target[key] === undefined && delete target[key]);
    return target;
}
exports.convert = convert;
function execute(endpoint) {
    return (event, context, cb) => __awaiter(this, void 0, void 0, function* () {
        try {
            const result = yield endpoint(event, context, cb);
            return callback(result);
        }
        catch (e) {
            return failure(e);
        }
    });
}
exports.execute = execute;
function callback(response) {
    return {
        body: JSON.stringify(response.entity),
        headers: baseHeaders(),
        statusCode: response.status,
    };
}
exports.callback = callback;
function failure(response) {
    if (response && isResponse(response)) {
        return callback(response);
    }
    return callback(error(http_status_codes_1.INTERNAL_SERVER_ERROR, error_codes_1.ERR_UNKNOWN_MSG, [{
            code: error_codes_1.ERR_UNKNOWN,
            description: response.message
                ? response.message
                : error_codes_1.ERR_REQUIRED_MSG,
        }]));
}
exports.failure = failure;
function isResponse(response) {
    return response.attributeId !== undefined;
}
exports.isResponse = isResponse;
function getSqsAttribute(event, attrKey) {
    if (event.Records) {
        const record = event.Records[0];
        if (record && record.messageAttributes) {
            const attributes = record.messageAttributes;
            const attrVal = attributes[attrKey];
            return attrVal ? (attrVal.stringValue || attrVal.StringValue) : undefined;
        }
    }
}
exports.getSqsAttribute = getSqsAttribute;
function getDirname() {
    const { LAMBDA_TASK_ROOT } = process.env;
    return LAMBDA_TASK_ROOT
        ? `${LAMBDA_TASK_ROOT}`
        : `${__dirname.split("/node_modules/")[0]}`;
}
exports.getDirname = getDirname;
//# sourceMappingURL=http-lambda.js.map