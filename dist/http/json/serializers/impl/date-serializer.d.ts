import { ISerializer } from "../serializer";
export declare class DateSerializer implements ISerializer<Date, string> {
    serialize(source: Date): string;
}
