"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const moment = require("moment-timezone");
const TZ = process.env.TZ || "America/Sao_Paulo";
class DateSerializer {
    serialize(source) {
        if (source) {
            return moment(source)
                .tz(TZ)
                .format("YYYY-MM-DDTHH:mm:ss");
        }
    }
}
exports.DateSerializer = DateSerializer;
//# sourceMappingURL=date-serializer.js.map