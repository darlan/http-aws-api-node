"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_request_1 = require("./http-request");
class Authentication {
    constructor(username, password, endpoint) {
        this.username = username;
        this.password = password;
        this.endpoint = endpoint;
    }
    authenticate(executable) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return yield this.execute(executable);
            }
            catch (e) {
                this.token = null;
                return yield this.execute(executable);
            }
        });
    }
    get token() {
        return this._token;
    }
    set token(token) {
        this._token = token;
    }
    execute(executable) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield this.getToken();
            return yield executable(token);
        });
    }
    getToken() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.token) {
                const { body } = (yield http_request_1.request().post(this.endpoint, {
                    body: {
                        login: this.username,
                        password: this.password,
                    }, json: true,
                }));
                this.token = body.accessToken;
            }
            return this.token;
        });
    }
}
exports.Authentication = Authentication;
//# sourceMappingURL=authentication.js.map